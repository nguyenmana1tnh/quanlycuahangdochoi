﻿
namespace QuanLyCuaHangDoChoi
{
    partial class frmNV
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lbThemNhanVien = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnUpdateNV = new System.Windows.Forms.Button();
            this.btnAddNV = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtGtNV = new System.Windows.Forms.TextBox();
            this.txtChucvuNV = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtNamsinhNV = new System.Windows.Forms.TextBox();
            this.txtDiachiNV = new System.Windows.Forms.TextBox();
            this.txtHotenNV = new System.Windows.Forms.TextBox();
            this.txtMaNV = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbMaNhanVien = new System.Windows.Forms.Label();
            this.txtSdtNV = new System.Windows.Forms.TextBox();
            this.panel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbThemNhanVien);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(532, 70);
            this.panel1.TabIndex = 1;
            // 
            // lbThemNhanVien
            // 
            this.lbThemNhanVien.AutoSize = true;
            this.lbThemNhanVien.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbThemNhanVien.ForeColor = System.Drawing.SystemColors.Highlight;
            this.lbThemNhanVien.Location = new System.Drawing.Point(128, 18);
            this.lbThemNhanVien.Name = "lbThemNhanVien";
            this.lbThemNhanVien.Size = new System.Drawing.Size(262, 38);
            this.lbThemNhanVien.TabIndex = 0;
            this.lbThemNhanVien.Text = "THÊM NHÂN VIÊN";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnUpdateNV);
            this.panel3.Controls.Add(this.btnAddNV);
            this.panel3.Location = new System.Drawing.Point(12, 506);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(532, 81);
            this.panel3.TabIndex = 3;
            // 
            // btnUpdateNV
            // 
            this.btnUpdateNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnUpdateNV.Location = new System.Drawing.Point(22, 16);
            this.btnUpdateNV.Name = "btnUpdateNV";
            this.btnUpdateNV.Size = new System.Drawing.Size(246, 45);
            this.btnUpdateNV.TabIndex = 1;
            this.btnUpdateNV.Text = "CẬP NHẬT THÔNG TIN";
            this.btnUpdateNV.UseVisualStyleBackColor = true;
            this.btnUpdateNV.Click += new System.EventHandler(this.btnUpdateNV_Click);
            // 
            // btnAddNV
            // 
            this.btnAddNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAddNV.Location = new System.Drawing.Point(281, 16);
            this.btnAddNV.Name = "btnAddNV";
            this.btnAddNV.Size = new System.Drawing.Size(246, 45);
            this.btnAddNV.TabIndex = 0;
            this.btnAddNV.Text = "THÊM NHÂN VIÊN";
            this.btnAddNV.UseVisualStyleBackColor = true;
            this.btnAddNV.Click += new System.EventHandler(this.btnAddNV_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtSdtNV);
            this.panel2.Controls.Add(this.txtGtNV);
            this.panel2.Controls.Add(this.txtChucvuNV);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.txtNamsinhNV);
            this.panel2.Controls.Add(this.txtDiachiNV);
            this.panel2.Controls.Add(this.txtHotenNV);
            this.panel2.Controls.Add(this.txtMaNV);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label2);
            this.panel2.Controls.Add(this.label1);
            this.panel2.Controls.Add(this.lbMaNhanVien);
            this.panel2.Location = new System.Drawing.Point(13, 100);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(532, 389);
            this.panel2.TabIndex = 4;
            // 
            // txtGtNV
            // 
            this.txtGtNV.Location = new System.Drawing.Point(183, 125);
            this.txtGtNV.Name = "txtGtNV";
            this.txtGtNV.Size = new System.Drawing.Size(334, 27);
            this.txtGtNV.TabIndex = 14;
            // 
            // txtChucvuNV
            // 
            this.txtChucvuNV.Location = new System.Drawing.Point(183, 180);
            this.txtChucvuNV.Name = "txtChucvuNV";
            this.txtChucvuNV.Size = new System.Drawing.Size(334, 27);
            this.txtChucvuNV.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(26, 179);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 28);
            this.label6.TabIndex = 12;
            this.label6.Text = "Chức vụ:";
            // 
            // txtNamsinhNV
            // 
            this.txtNamsinhNV.Location = new System.Drawing.Point(183, 226);
            this.txtNamsinhNV.Name = "txtNamsinhNV";
            this.txtNamsinhNV.Size = new System.Drawing.Size(334, 27);
            this.txtNamsinhNV.TabIndex = 11;
            // 
            // txtDiachiNV
            // 
            this.txtDiachiNV.Location = new System.Drawing.Point(183, 333);
            this.txtDiachiNV.Name = "txtDiachiNV";
            this.txtDiachiNV.Size = new System.Drawing.Size(334, 27);
            this.txtDiachiNV.TabIndex = 9;
            // 
            // txtHotenNV
            // 
            this.txtHotenNV.Location = new System.Drawing.Point(183, 75);
            this.txtHotenNV.Name = "txtHotenNV";
            this.txtHotenNV.Size = new System.Drawing.Size(334, 27);
            this.txtHotenNV.TabIndex = 7;
            // 
            // txtMaNV
            // 
            this.txtMaNV.Location = new System.Drawing.Point(183, 23);
            this.txtMaNV.Name = "txtMaNV";
            this.txtMaNV.Size = new System.Drawing.Size(334, 27);
            this.txtMaNV.TabIndex = 6;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(25, 329);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(83, 28);
            this.label5.TabIndex = 5;
            this.label5.Text = "Địa chỉ:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(25, 71);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 28);
            this.label4.TabIndex = 4;
            this.label4.Text = "Họ và Tên";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(25, 124);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 28);
            this.label3.TabIndex = 3;
            this.label3.Text = "Giới tính:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(25, 222);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 28);
            this.label2.TabIndex = 2;
            this.label2.Text = "Năm sinh:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(25, 276);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 28);
            this.label1.TabIndex = 1;
            this.label1.Text = "Số điện thoại: ";
            // 
            // lbMaNhanVien
            // 
            this.lbMaNhanVien.AutoSize = true;
            this.lbMaNhanVien.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.lbMaNhanVien.Location = new System.Drawing.Point(25, 22);
            this.lbMaNhanVien.Name = "lbMaNhanVien";
            this.lbMaNhanVien.Size = new System.Drawing.Size(152, 28);
            this.lbMaNhanVien.TabIndex = 0;
            this.lbMaNhanVien.Text = "Mã nhân viên: ";
            // 
            // txtSdtNV
            // 
            this.txtSdtNV.Location = new System.Drawing.Point(183, 280);
            this.txtSdtNV.Name = "txtSdtNV";
            this.txtSdtNV.Size = new System.Drawing.Size(334, 27);
            this.txtSdtNV.TabIndex = 15;
            // 
            // frmNV
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(557, 600);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel1);
            this.Name = "frmNV";
            this.Text = "frmNV";
            this.Load += new System.EventHandler(this.frmNV_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label lbThemNhanVien;
        private System.Windows.Forms.Panel panel3;
        
        
        private System.Windows.Forms.Button btnUpdateNV;
        private System.Windows.Forms.Button btnAddNV;
        
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtGioiTinh;
        private System.Windows.Forms.TextBox txtChucVu;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtNamSinh;
        private System.Windows.Forms.TextBox txtDiachiNV;
        private System.Windows.Forms.TextBox txtSoDienThoai;
        private System.Windows.Forms.TextBox txtHoVaTen;
        private System.Windows.Forms.TextBox txtMaNV;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbMaNhanVien;
        private System.Windows.Forms.TextBox txtHotenNV;
        private System.Windows.Forms.TextBox txtGtNV;
        private System.Windows.Forms.TextBox txtChucvuNV;
        private System.Windows.Forms.TextBox txtNamsinhNV;
        private System.Windows.Forms.TextBox txtSdtNV;
    }
}