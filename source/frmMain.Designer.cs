﻿
namespace QuanLyCuaHangDoChoi
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnChangeAccount = new System.Windows.Forms.Button();
            this.lbStoreName = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnHD = new System.Windows.Forms.Button();
            this.btnHH = new System.Windows.Forms.Button();
            this.btnTK = new System.Windows.Forms.Button();
            this.btnNH = new System.Windows.Forms.Button();
            this.btnNV = new System.Windows.Forms.Button();
            this.pnNV = new System.Windows.Forms.Panel();
            this.btnTimNV = new System.Windows.Forms.Button();
            this.txtTimNV = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnXoaNV = new System.Windows.Forms.Button();
            this.btnSuaNV = new System.Windows.Forms.Button();
            this.btnThemNV = new System.Windows.Forms.Button();
            this.btnXemNV = new System.Windows.Forms.Button();
            this.dgvNV = new System.Windows.Forms.DataGridView();
            this.clMaNV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clHoTenNV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clGioiTinh = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clChucVuNV = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnNH = new System.Windows.Forms.Panel();
            this.btnXoaNH = new System.Windows.Forms.Button();
            this.btnSuaNH = new System.Windows.Forms.Button();
            this.btnThemNH = new System.Windows.Forms.Button();
            this.btnXemNH = new System.Windows.Forms.Button();
            this.dgvNH = new System.Windows.Forms.DataGridView();
            this.clMaNH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clNgayNH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clTongTienNH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnTK = new System.Windows.Forms.Panel();
            this.btnXoaTK = new System.Windows.Forms.Button();
            this.btnSuaTK = new System.Windows.Forms.Button();
            this.btnThemTK = new System.Windows.Forms.Button();
            this.btnXemTK = new System.Windows.Forms.Button();
            this.dgvTK = new System.Windows.Forms.DataGridView();
            this.clIDTK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clNgayTK = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clDoanhThu = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnHH = new System.Windows.Forms.Panel();
            this.btnTimHH = new System.Windows.Forms.Button();
            this.txtTimHH = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnXoaHH = new System.Windows.Forms.Button();
            this.btnThemHH = new System.Windows.Forms.Button();
            this.dgvHH = new System.Windows.Forms.DataGridView();
            this.clMaHH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clTenHH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clSLHH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnSuaHH = new System.Windows.Forms.Button();
            this.btnXemHH = new System.Windows.Forms.Button();
            this.pnHD = new System.Windows.Forms.Panel();
            this.btnTimHD = new System.Windows.Forms.Button();
            this.txtTimHD = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnXoaHD = new System.Windows.Forms.Button();
            this.btnThemHD = new System.Windows.Forms.Button();
            this.btnSuaHD = new System.Windows.Forms.Button();
            this.dgvHD = new System.Windows.Forms.DataGridView();
            this.clSoHD = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clNgayLap = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clTongTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnXemHD = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.pnNV.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNV)).BeginInit();
            this.pnNH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNH)).BeginInit();
            this.pnTK.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTK)).BeginInit();
            this.pnHH.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHH)).BeginInit();
            this.pnHD.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHD)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnChangeAccount);
            this.panel2.Controls.Add(this.lbStoreName);
            this.panel2.Location = new System.Drawing.Point(12, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1166, 71);
            this.panel2.TabIndex = 2;
            // 
            // btnChangeAccount
            // 
            this.btnChangeAccount.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnChangeAccount.ForeColor = System.Drawing.Color.Red;
            this.btnChangeAccount.Location = new System.Drawing.Point(945, 11);
            this.btnChangeAccount.Name = "btnChangeAccount";
            this.btnChangeAccount.Size = new System.Drawing.Size(201, 57);
            this.btnChangeAccount.TabIndex = 1;
            this.btnChangeAccount.Text = "ĐỔI TÀI KHOẢN";
            this.btnChangeAccount.UseVisualStyleBackColor = true;
            this.btnChangeAccount.Click += new System.EventHandler(this.btnChangeAccount_Click);
            // 
            // lbStoreName
            // 
            this.lbStoreName.AutoSize = true;
            this.lbStoreName.Font = new System.Drawing.Font("Times New Roman", 22.2F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point);
            this.lbStoreName.ForeColor = System.Drawing.Color.Blue;
            this.lbStoreName.Location = new System.Drawing.Point(14, 14);
            this.lbStoreName.Name = "lbStoreName";
            this.lbStoreName.Size = new System.Drawing.Size(455, 43);
            this.lbStoreName.TabIndex = 0;
            this.lbStoreName.Text = "CỬA HÀNG ĐỒ CHƠI MM";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Controls.Add(this.btnHD);
            this.panel3.Controls.Add(this.btnHH);
            this.panel3.Controls.Add(this.btnTK);
            this.panel3.Controls.Add(this.btnNH);
            this.panel3.Controls.Add(this.btnNV);
            this.panel3.Location = new System.Drawing.Point(12, 98);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(262, 559);
            this.panel3.TabIndex = 3;
            // 
            // btnHD
            // 
            this.btnHD.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHD.ForeColor = System.Drawing.Color.Blue;
            this.btnHD.Location = new System.Drawing.Point(14, 395);
            this.btnHD.Name = "btnHD";
            this.btnHD.Size = new System.Drawing.Size(230, 75);
            this.btnHD.TabIndex = 4;
            this.btnHD.Text = "Hóa Đơn";
            this.btnHD.UseVisualStyleBackColor = true;
            this.btnHD.Click += new System.EventHandler(this.btnHD_Click);
            // 
            // btnHH
            // 
            this.btnHH.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnHH.ForeColor = System.Drawing.Color.Blue;
            this.btnHH.Location = new System.Drawing.Point(14, 306);
            this.btnHH.Name = "btnHH";
            this.btnHH.Size = new System.Drawing.Size(230, 77);
            this.btnHH.TabIndex = 3;
            this.btnHH.Text = "Hàng Hóa";
            this.btnHH.UseVisualStyleBackColor = true;
            this.btnHH.Click += new System.EventHandler(this.btnHH_Click);
            // 
            // btnTK
            // 
            this.btnTK.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTK.ForeColor = System.Drawing.Color.Blue;
            this.btnTK.Location = new System.Drawing.Point(14, 218);
            this.btnTK.Name = "btnTK";
            this.btnTK.Size = new System.Drawing.Size(230, 71);
            this.btnTK.TabIndex = 2;
            this.btnTK.Text = "Thống Kê";
            this.btnTK.UseVisualStyleBackColor = true;
            this.btnTK.Click += new System.EventHandler(this.btnTK_Click);
            // 
            // btnNH
            // 
            this.btnNH.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnNH.ForeColor = System.Drawing.Color.Blue;
            this.btnNH.Location = new System.Drawing.Point(14, 126);
            this.btnNH.Name = "btnNH";
            this.btnNH.Size = new System.Drawing.Size(230, 77);
            this.btnNH.TabIndex = 1;
            this.btnNH.Text = "Nhập Hàng";
            this.btnNH.UseVisualStyleBackColor = true;
            this.btnNH.Click += new System.EventHandler(this.btnNH_Click);
            // 
            // btnNV
            // 
            this.btnNV.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnNV.ForeColor = System.Drawing.Color.Blue;
            this.btnNV.Location = new System.Drawing.Point(14, 28);
            this.btnNV.Name = "btnNV";
            this.btnNV.Size = new System.Drawing.Size(230, 77);
            this.btnNV.TabIndex = 0;
            this.btnNV.Text = "Nhân Viên";
            this.btnNV.UseVisualStyleBackColor = true;
            this.btnNV.Click += new System.EventHandler(this.btnNV_Click);
            // 
            // pnNV
            // 
            this.pnNV.Controls.Add(this.btnTimNV);
            this.pnNV.Controls.Add(this.txtTimNV);
            this.pnNV.Controls.Add(this.label1);
            this.pnNV.Controls.Add(this.btnXoaNV);
            this.pnNV.Controls.Add(this.btnSuaNV);
            this.pnNV.Controls.Add(this.btnThemNV);
            this.pnNV.Controls.Add(this.btnXemNV);
            this.pnNV.Controls.Add(this.dgvNV);
            this.pnNV.Location = new System.Drawing.Point(300, 100);
            this.pnNV.Name = "pnNV";
            this.pnNV.Size = new System.Drawing.Size(888, 559);
            this.pnNV.TabIndex = 7;
            // 
            // btnTimNV
            // 
            this.btnTimNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTimNV.Location = new System.Drawing.Point(667, 393);
            this.btnTimNV.Name = "btnTimNV";
            this.btnTimNV.Size = new System.Drawing.Size(209, 46);
            this.btnTimNV.TabIndex = 17;
            this.btnTimNV.Text = "TÌM KIẾM";
            this.btnTimNV.UseVisualStyleBackColor = true;
            this.btnTimNV.Click += new System.EventHandler(this.btnTimNV_Click);
            // 
            // txtTimNV
            // 
            this.txtTimNV.Location = new System.Drawing.Point(291, 404);
            this.txtTimNV.Name = "txtTimNV";
            this.txtTimNV.Size = new System.Drawing.Size(357, 27);
            this.txtTimNV.TabIndex = 16;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(10, 400);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(275, 31);
            this.label1.TabIndex = 15;
            this.label1.Text = "Nhập nhân viên cần tìm:";
            // 
            // btnXoaNV
            // 
            this.btnXoaNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXoaNV.Location = new System.Drawing.Point(667, 462);
            this.btnXoaNV.Name = "btnXoaNV";
            this.btnXoaNV.Size = new System.Drawing.Size(209, 67);
            this.btnXoaNV.TabIndex = 8;
            this.btnXoaNV.Text = "XÓA NHÂN VIÊN";
            this.btnXoaNV.UseVisualStyleBackColor = true;
            this.btnXoaNV.Click += new System.EventHandler(this.btnXoaNV_Click);
            // 
            // btnSuaNV
            // 
            this.btnSuaNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSuaNV.Location = new System.Drawing.Point(442, 462);
            this.btnSuaNV.Name = "btnSuaNV";
            this.btnSuaNV.Size = new System.Drawing.Size(194, 67);
            this.btnSuaNV.TabIndex = 7;
            this.btnSuaNV.Text = "SỬA THÔNG TIN NHÂN VIÊN";
            this.btnSuaNV.UseVisualStyleBackColor = true;
            this.btnSuaNV.Click += new System.EventHandler(this.btnSuaNV_Click);
            // 
            // btnThemNV
            // 
            this.btnThemNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnThemNV.Location = new System.Drawing.Point(19, 462);
            this.btnThemNV.Name = "btnThemNV";
            this.btnThemNV.Size = new System.Drawing.Size(165, 67);
            this.btnThemNV.TabIndex = 5;
            this.btnThemNV.Text = "THÊM NHÂN VIÊN";
            this.btnThemNV.UseVisualStyleBackColor = true;
            this.btnThemNV.Click += new System.EventHandler(this.btnThemNV_Click);
            // 
            // btnXemNV
            // 
            this.btnXemNV.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXemNV.Location = new System.Drawing.Point(228, 462);
            this.btnXemNV.Name = "btnXemNV";
            this.btnXemNV.Size = new System.Drawing.Size(183, 67);
            this.btnXemNV.TabIndex = 6;
            this.btnXemNV.Text = "XEM THÔNG TIN NHÂN VIÊN";
            this.btnXemNV.UseVisualStyleBackColor = true;
            this.btnXemNV.Click += new System.EventHandler(this.btnXemNV_Click);
            // 
            // dgvNV
            // 
            this.dgvNV.AllowUserToAddRows = false;
            this.dgvNV.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNV.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clMaNV,
            this.clHoTenNV,
            this.clGioiTinh,
            this.clChucVuNV});
            this.dgvNV.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvNV.Location = new System.Drawing.Point(0, 0);
            this.dgvNV.Name = "dgvNV";
            this.dgvNV.ReadOnly = true;
            this.dgvNV.RowHeadersWidth = 51;
            this.dgvNV.RowTemplate.Height = 29;
            this.dgvNV.Size = new System.Drawing.Size(888, 362);
            this.dgvNV.TabIndex = 6;
            this.dgvNV.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNV_CellClick);
            // 
            // clMaNV
            // 
            this.clMaNV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clMaNV.DataPropertyName = "MaNV";
            this.clMaNV.HeaderText = "ID";
            this.clMaNV.MinimumWidth = 6;
            this.clMaNV.Name = "clMaNV";
            this.clMaNV.ReadOnly = true;
            this.clMaNV.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clMaNV.Width = 53;
            // 
            // clHoTenNV
            // 
            this.clHoTenNV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clHoTenNV.DataPropertyName = "Hoten";
            this.clHoTenNV.HeaderText = "Họ và Tên";
            this.clHoTenNV.MinimumWidth = 6;
            this.clHoTenNV.Name = "clHoTenNV";
            this.clHoTenNV.ReadOnly = true;
            this.clHoTenNV.Width = 104;
            // 
            // clGioiTinh
            // 
            this.clGioiTinh.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clGioiTinh.DataPropertyName = "Gioi";
            this.clGioiTinh.HeaderText = "Giới Tính";
            this.clGioiTinh.MinimumWidth = 6;
            this.clGioiTinh.Name = "clGioiTinh";
            this.clGioiTinh.ReadOnly = true;
            this.clGioiTinh.Width = 97;
            // 
            // clChucVuNV
            // 
            this.clChucVuNV.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clChucVuNV.DataPropertyName = "ChucVu";
            this.clChucVuNV.HeaderText = "Chức Vụ";
            this.clChucVuNV.MinimumWidth = 6;
            this.clChucVuNV.Name = "clChucVuNV";
            this.clChucVuNV.ReadOnly = true;
            // 
            // pnNH
            // 
            this.pnNH.Controls.Add(this.btnXoaNH);
            this.pnNH.Controls.Add(this.btnSuaNH);
            this.pnNH.Controls.Add(this.btnThemNH);
            this.pnNH.Controls.Add(this.btnXemNH);
            this.pnNH.Controls.Add(this.dgvNH);
            this.pnNH.Location = new System.Drawing.Point(300, 98);
            this.pnNH.Name = "pnNH";
            this.pnNH.Size = new System.Drawing.Size(888, 559);
            this.pnNH.TabIndex = 9;
            // 
            // btnXoaNH
            // 
            this.btnXoaNH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXoaNH.Location = new System.Drawing.Point(664, 466);
            this.btnXoaNH.Name = "btnXoaNH";
            this.btnXoaNH.Size = new System.Drawing.Size(209, 67);
            this.btnXoaNH.TabIndex = 8;
            this.btnXoaNH.Text = "XÓA ĐƠN NHẬP HÀNG";
            this.btnXoaNH.UseVisualStyleBackColor = true;
            this.btnXoaNH.Click += new System.EventHandler(this.btnXoaNH_Click);
            // 
            // btnSuaNH
            // 
            this.btnSuaNH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSuaNH.Location = new System.Drawing.Point(442, 466);
            this.btnSuaNH.Name = "btnSuaNH";
            this.btnSuaNH.Size = new System.Drawing.Size(194, 67);
            this.btnSuaNH.TabIndex = 7;
            this.btnSuaNH.Text = "SỬA ĐƠN NHẬP HÀNG";
            this.btnSuaNH.UseVisualStyleBackColor = true;
            this.btnSuaNH.Click += new System.EventHandler(this.btnSuaNH_Click);
            // 
            // btnThemNH
            // 
            this.btnThemNH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnThemNH.Location = new System.Drawing.Point(28, 466);
            this.btnThemNH.Name = "btnThemNH";
            this.btnThemNH.Size = new System.Drawing.Size(165, 67);
            this.btnThemNH.TabIndex = 5;
            this.btnThemNH.Text = "THÊM ĐƠN NHẬP HÀNG";
            this.btnThemNH.UseVisualStyleBackColor = true;
            this.btnThemNH.Click += new System.EventHandler(this.btnThemNH_Click);
            // 
            // btnXemNH
            // 
            this.btnXemNH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXemNH.Location = new System.Drawing.Point(228, 466);
            this.btnXemNH.Name = "btnXemNH";
            this.btnXemNH.Size = new System.Drawing.Size(183, 67);
            this.btnXemNH.TabIndex = 6;
            this.btnXemNH.Text = "XEM ĐƠN NHẬP HÀNG";
            this.btnXemNH.UseVisualStyleBackColor = true;
            this.btnXemNH.Click += new System.EventHandler(this.btnXemNH_Click);
            // 
            // dgvNH
            // 
            this.dgvNH.AllowUserToAddRows = false;
            this.dgvNH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvNH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clMaNH,
            this.clNgayNH,
            this.clTongTienNH});
            this.dgvNH.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvNH.Location = new System.Drawing.Point(0, 0);
            this.dgvNH.Name = "dgvNH";
            this.dgvNH.ReadOnly = true;
            this.dgvNH.RowHeadersWidth = 51;
            this.dgvNH.RowTemplate.Height = 29;
            this.dgvNH.Size = new System.Drawing.Size(888, 362);
            this.dgvNH.TabIndex = 6;
            this.dgvNH.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvNH_CellClick);
            // 
            // clMaNH
            // 
            this.clMaNH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clMaNH.DataPropertyName = "MaNH";
            this.clMaNH.HeaderText = "Mã";
            this.clMaNH.MinimumWidth = 6;
            this.clMaNH.Name = "clMaNH";
            this.clMaNH.ReadOnly = true;
            this.clMaNH.Width = 59;
            // 
            // clNgayNH
            // 
            this.clNgayNH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clNgayNH.DataPropertyName = "NgayNH";
            this.clNgayNH.HeaderText = "Ngày";
            this.clNgayNH.MinimumWidth = 6;
            this.clNgayNH.Name = "clNgayNH";
            this.clNgayNH.ReadOnly = true;
            this.clNgayNH.Width = 73;
            // 
            // clTongTienNH
            // 
            this.clTongTienNH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clTongTienNH.DataPropertyName = "ChiPhi";
            this.clTongTienNH.HeaderText = "Chi phí";
            this.clTongTienNH.MinimumWidth = 6;
            this.clTongTienNH.Name = "clTongTienNH";
            this.clTongTienNH.ReadOnly = true;
            // 
            // pnTK
            // 
            this.pnTK.Controls.Add(this.btnXoaTK);
            this.pnTK.Controls.Add(this.btnSuaTK);
            this.pnTK.Controls.Add(this.btnThemTK);
            this.pnTK.Controls.Add(this.btnXemTK);
            this.pnTK.Controls.Add(this.dgvTK);
            this.pnTK.Location = new System.Drawing.Point(300, 98);
            this.pnTK.Name = "pnTK";
            this.pnTK.Size = new System.Drawing.Size(888, 559);
            this.pnTK.TabIndex = 10;
            // 
            // btnXoaTK
            // 
            this.btnXoaTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXoaTK.Location = new System.Drawing.Point(658, 469);
            this.btnXoaTK.Name = "btnXoaTK";
            this.btnXoaTK.Size = new System.Drawing.Size(209, 67);
            this.btnXoaTK.TabIndex = 8;
            this.btnXoaTK.Text = "XÓA THỐNG KÊ";
            this.btnXoaTK.UseVisualStyleBackColor = true;
            this.btnXoaTK.Click += new System.EventHandler(this.btnXoaTK_Click);
            // 
            // btnSuaTK
            // 
            this.btnSuaTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSuaTK.Location = new System.Drawing.Point(437, 469);
            this.btnSuaTK.Name = "btnSuaTK";
            this.btnSuaTK.Size = new System.Drawing.Size(194, 67);
            this.btnSuaTK.TabIndex = 7;
            this.btnSuaTK.Text = "SỬA THỐNG KÊ";
            this.btnSuaTK.UseVisualStyleBackColor = true;
            this.btnSuaTK.Click += new System.EventHandler(this.btnSuaTK_Click);
            // 
            // btnThemTK
            // 
            this.btnThemTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnThemTK.Location = new System.Drawing.Point(24, 469);
            this.btnThemTK.Name = "btnThemTK";
            this.btnThemTK.Size = new System.Drawing.Size(165, 67);
            this.btnThemTK.TabIndex = 5;
            this.btnThemTK.Text = "THÊM THỐNG KÊ";
            this.btnThemTK.UseVisualStyleBackColor = true;
            this.btnThemTK.Click += new System.EventHandler(this.btnThemTK_Click);
            // 
            // btnXemTK
            // 
            this.btnXemTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXemTK.Location = new System.Drawing.Point(219, 469);
            this.btnXemTK.Name = "btnXemTK";
            this.btnXemTK.Size = new System.Drawing.Size(183, 67);
            this.btnXemTK.TabIndex = 6;
            this.btnXemTK.Text = "XEM THỐNG KÊ";
            this.btnXemTK.UseVisualStyleBackColor = true;
            this.btnXemTK.Click += new System.EventHandler(this.btnXemTK_Click);
            // 
            // dgvTK
            // 
            this.dgvTK.AllowUserToAddRows = false;
            this.dgvTK.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTK.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clIDTK,
            this.clNgayTK,
            this.clDoanhThu});
            this.dgvTK.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvTK.Location = new System.Drawing.Point(0, 0);
            this.dgvTK.Name = "dgvTK";
            this.dgvTK.ReadOnly = true;
            this.dgvTK.RowHeadersWidth = 51;
            this.dgvTK.RowTemplate.Height = 29;
            this.dgvTK.Size = new System.Drawing.Size(888, 362);
            this.dgvTK.TabIndex = 6;
            this.dgvTK.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTK_CellClick);
            // 
            // clIDTK
            // 
            this.clIDTK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clIDTK.DataPropertyName = "ID";
            this.clIDTK.HeaderText = "ID";
            this.clIDTK.MinimumWidth = 6;
            this.clIDTK.Name = "clIDTK";
            this.clIDTK.ReadOnly = true;
            this.clIDTK.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clIDTK.Width = 53;
            // 
            // clNgayTK
            // 
            this.clNgayTK.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clNgayTK.DataPropertyName = "NgayTK";
            this.clNgayTK.HeaderText = "Ngày thống kê";
            this.clNgayTK.MinimumWidth = 6;
            this.clNgayTK.Name = "clNgayTK";
            this.clNgayTK.ReadOnly = true;
            this.clNgayTK.Width = 110;
            // 
            // clDoanhThu
            // 
            this.clDoanhThu.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clDoanhThu.DataPropertyName = "DoanhThu";
            this.clDoanhThu.HeaderText = "Doanh thu";
            this.clDoanhThu.MinimumWidth = 6;
            this.clDoanhThu.Name = "clDoanhThu";
            this.clDoanhThu.ReadOnly = true;
            // 
            // pnHH
            // 
            this.pnHH.Controls.Add(this.btnTimHH);
            this.pnHH.Controls.Add(this.txtTimHH);
            this.pnHH.Controls.Add(this.label2);
            this.pnHH.Controls.Add(this.btnXoaHH);
            this.pnHH.Controls.Add(this.btnThemHH);
            this.pnHH.Controls.Add(this.dgvHH);
            this.pnHH.Controls.Add(this.btnSuaHH);
            this.pnHH.Controls.Add(this.btnXemHH);
            this.pnHH.Location = new System.Drawing.Point(300, 98);
            this.pnHH.Name = "pnHH";
            this.pnHH.Size = new System.Drawing.Size(888, 559);
            this.pnHH.TabIndex = 11;
            // 
            // btnTimHH
            // 
            this.btnTimHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTimHH.Location = new System.Drawing.Point(674, 390);
            this.btnTimHH.Name = "btnTimHH";
            this.btnTimHH.Size = new System.Drawing.Size(209, 46);
            this.btnTimHH.TabIndex = 14;
            this.btnTimHH.Text = "TÌM KIẾM";
            this.btnTimHH.UseVisualStyleBackColor = true;
            // 
            // txtTimHH
            // 
            this.txtTimHH.Location = new System.Drawing.Point(289, 401);
            this.txtTimHH.Name = "txtTimHH";
            this.txtTimHH.Size = new System.Drawing.Size(357, 27);
            this.txtTimHH.TabIndex = 13;
            this.txtTimHH.Click += new System.EventHandler(this.txtTimHH_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(23, 397);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(251, 31);
            this.label2.TabIndex = 12;
            this.label2.Text = "Nhập đồ chơi cần tìm:";
            // 
            // btnXoaHH
            // 
            this.btnXoaHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXoaHH.Location = new System.Drawing.Point(676, 468);
            this.btnXoaHH.Name = "btnXoaHH";
            this.btnXoaHH.Size = new System.Drawing.Size(209, 67);
            this.btnXoaHH.TabIndex = 8;
            this.btnXoaHH.Text = "XÓA HÀNG HÓA";
            this.btnXoaHH.UseVisualStyleBackColor = true;
            this.btnXoaHH.Click += new System.EventHandler(this.btnXoaHH_Click);
            // 
            // btnThemHH
            // 
            this.btnThemHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnThemHH.Location = new System.Drawing.Point(12, 468);
            this.btnThemHH.Name = "btnThemHH";
            this.btnThemHH.Size = new System.Drawing.Size(165, 67);
            this.btnThemHH.TabIndex = 5;
            this.btnThemHH.Text = "THÊM HÀNG HÓA";
            this.btnThemHH.UseVisualStyleBackColor = true;
            this.btnThemHH.Click += new System.EventHandler(this.btnThemHH_Click);
            // 
            // dgvHH
            // 
            this.dgvHH.AllowUserToAddRows = false;
            this.dgvHH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHH.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clMaHH,
            this.clTenHH,
            this.clSLHH});
            this.dgvHH.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvHH.Location = new System.Drawing.Point(0, 0);
            this.dgvHH.Name = "dgvHH";
            this.dgvHH.ReadOnly = true;
            this.dgvHH.RowHeadersWidth = 51;
            this.dgvHH.RowTemplate.Height = 29;
            this.dgvHH.Size = new System.Drawing.Size(888, 362);
            this.dgvHH.TabIndex = 6;
            this.dgvHH.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHH_CellClick);
            // 
            // clMaHH
            // 
            this.clMaHH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clMaHH.DataPropertyName = "MaHH";
            this.clMaHH.HeaderText = "Mã";
            this.clMaHH.MinimumWidth = 6;
            this.clMaHH.Name = "clMaHH";
            this.clMaHH.ReadOnly = true;
            this.clMaHH.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clMaHH.Width = 59;
            // 
            // clTenHH
            // 
            this.clTenHH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clTenHH.DataPropertyName = "TenHH";
            this.clTenHH.HeaderText = "Tên hàng hóa";
            this.clTenHH.MinimumWidth = 6;
            this.clTenHH.Name = "clTenHH";
            this.clTenHH.ReadOnly = true;
            this.clTenHH.Width = 127;
            // 
            // clSLHH
            // 
            this.clSLHH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clSLHH.DataPropertyName = "SL";
            this.clSLHH.HeaderText = "Số lượng";
            this.clSLHH.MinimumWidth = 6;
            this.clSLHH.Name = "clSLHH";
            this.clSLHH.ReadOnly = true;
            // 
            // btnSuaHH
            // 
            this.btnSuaHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSuaHH.Location = new System.Drawing.Point(440, 468);
            this.btnSuaHH.Name = "btnSuaHH";
            this.btnSuaHH.Size = new System.Drawing.Size(194, 67);
            this.btnSuaHH.TabIndex = 7;
            this.btnSuaHH.Text = "SỬA HÀNG HÓA";
            this.btnSuaHH.UseVisualStyleBackColor = true;
            this.btnSuaHH.Click += new System.EventHandler(this.btnSuaHH_Click);
            // 
            // btnXemHH
            // 
            this.btnXemHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXemHH.Location = new System.Drawing.Point(212, 468);
            this.btnXemHH.Name = "btnXemHH";
            this.btnXemHH.Size = new System.Drawing.Size(183, 67);
            this.btnXemHH.TabIndex = 6;
            this.btnXemHH.Text = "XEM HÀNG HÓA";
            this.btnXemHH.UseVisualStyleBackColor = true;
            this.btnXemHH.Click += new System.EventHandler(this.btnXemHH_Click);
            // 
            // pnHD
            // 
            this.pnHD.Controls.Add(this.btnTimHD);
            this.pnHD.Controls.Add(this.txtTimHD);
            this.pnHD.Controls.Add(this.label3);
            this.pnHD.Controls.Add(this.btnXoaHD);
            this.pnHD.Controls.Add(this.btnThemHD);
            this.pnHD.Controls.Add(this.btnSuaHD);
            this.pnHD.Controls.Add(this.dgvHD);
            this.pnHD.Controls.Add(this.btnXemHD);
            this.pnHD.Location = new System.Drawing.Point(300, 98);
            this.pnHD.Name = "pnHD";
            this.pnHD.Size = new System.Drawing.Size(888, 559);
            this.pnHD.TabIndex = 12;
            // 
            // btnTimHD
            // 
            this.btnTimHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnTimHD.Location = new System.Drawing.Point(669, 385);
            this.btnTimHD.Name = "btnTimHD";
            this.btnTimHD.Size = new System.Drawing.Size(209, 46);
            this.btnTimHD.TabIndex = 11;
            this.btnTimHD.Text = "TÌM KIẾM";
            this.btnTimHD.UseVisualStyleBackColor = true;
            // 
            // txtTimHD
            // 
            this.txtTimHD.Location = new System.Drawing.Point(284, 396);
            this.txtTimHD.Name = "txtTimHD";
            this.txtTimHD.Size = new System.Drawing.Size(357, 27);
            this.txtTimHD.TabIndex = 10;
            this.txtTimHD.Click += new System.EventHandler(this.txtTimHD_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(18, 392);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(259, 31);
            this.label3.TabIndex = 9;
            this.label3.Text = "Nhập hóa đơn cần tìm:";
            // 
            // btnXoaHD
            // 
            this.btnXoaHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXoaHD.Location = new System.Drawing.Point(669, 474);
            this.btnXoaHD.Name = "btnXoaHD";
            this.btnXoaHD.Size = new System.Drawing.Size(209, 67);
            this.btnXoaHD.TabIndex = 8;
            this.btnXoaHD.Text = "XÓA HÓA ĐƠN";
            this.btnXoaHD.UseVisualStyleBackColor = true;
            this.btnXoaHD.Click += new System.EventHandler(this.btnXoaHD_Click);
            // 
            // btnThemHD
            // 
            this.btnThemHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnThemHD.Location = new System.Drawing.Point(18, 474);
            this.btnThemHD.Name = "btnThemHD";
            this.btnThemHD.Size = new System.Drawing.Size(165, 67);
            this.btnThemHD.TabIndex = 5;
            this.btnThemHD.Text = "THÊM HÓA ĐƠN";
            this.btnThemHD.UseVisualStyleBackColor = true;
            this.btnThemHD.Click += new System.EventHandler(this.btnThemHD_Click);
            // 
            // btnSuaHD
            // 
            this.btnSuaHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnSuaHD.Location = new System.Drawing.Point(431, 474);
            this.btnSuaHD.Name = "btnSuaHD";
            this.btnSuaHD.Size = new System.Drawing.Size(194, 67);
            this.btnSuaHD.TabIndex = 7;
            this.btnSuaHD.Text = "SỬA HÓA ĐƠN";
            this.btnSuaHD.UseVisualStyleBackColor = true;
            this.btnSuaHD.Click += new System.EventHandler(this.btnSuaHD_Click);
            // 
            // dgvHD
            // 
            this.dgvHD.AllowUserToAddRows = false;
            this.dgvHD.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHD.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clSoHD,
            this.clNgayLap,
            this.clTongTien});
            this.dgvHD.Dock = System.Windows.Forms.DockStyle.Top;
            this.dgvHD.Location = new System.Drawing.Point(0, 0);
            this.dgvHD.Name = "dgvHD";
            this.dgvHD.ReadOnly = true;
            this.dgvHD.RowHeadersWidth = 51;
            this.dgvHD.RowTemplate.Height = 29;
            this.dgvHD.Size = new System.Drawing.Size(888, 362);
            this.dgvHD.TabIndex = 6;
            this.dgvHD.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvHD_CellClick);
            // 
            // clSoHD
            // 
            this.clSoHD.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clSoHD.DataPropertyName = "SoHD";
            this.clSoHD.HeaderText = "Số hóa đơn";
            this.clSoHD.MinimumWidth = 6;
            this.clSoHD.Name = "clSoHD";
            this.clSoHD.ReadOnly = true;
            this.clSoHD.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.clSoHD.Width = 114;
            // 
            // clNgayLap
            // 
            this.clNgayLap.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clNgayLap.DataPropertyName = "NgayLap";
            this.clNgayLap.HeaderText = "Ngày lập";
            this.clNgayLap.MinimumWidth = 6;
            this.clNgayLap.Name = "clNgayLap";
            this.clNgayLap.ReadOnly = true;
            this.clNgayLap.Width = 98;
            // 
            // clTongTien
            // 
            this.clTongTien.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clTongTien.DataPropertyName = "TongTien";
            this.clTongTien.HeaderText = "Tổng tiền";
            this.clTongTien.MinimumWidth = 6;
            this.clTongTien.Name = "clTongTien";
            this.clTongTien.ReadOnly = true;
            // 
            // btnXemHD
            // 
            this.btnXemHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnXemHD.Location = new System.Drawing.Point(210, 474);
            this.btnXemHD.Name = "btnXemHD";
            this.btnXemHD.Size = new System.Drawing.Size(183, 67);
            this.btnXemHD.TabIndex = 6;
            this.btnXemHD.Text = "XEM HÓA ĐƠN";
            this.btnXemHD.UseVisualStyleBackColor = true;
            this.btnXemHD.Click += new System.EventHandler(this.btnXemHD_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1200, 671);
            this.Controls.Add(this.pnHD);
            this.Controls.Add(this.pnHH);
            this.Controls.Add(this.pnTK);
            this.Controls.Add(this.pnNH);
            this.Controls.Add(this.pnNV);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMain";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMain_FormClosed);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.pnNV.ResumeLayout(false);
            this.pnNV.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvNV)).EndInit();
            this.pnNH.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvNH)).EndInit();
            this.pnTK.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvTK)).EndInit();
            this.pnHH.ResumeLayout(false);
            this.pnHH.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHH)).EndInit();
            this.pnHD.ResumeLayout(false);
            this.pnHD.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHD)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnChangeAccount;
        private System.Windows.Forms.Label lbStoreName;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnHD;
        private System.Windows.Forms.Button btnHH;
        private System.Windows.Forms.Button btnTK;
        private System.Windows.Forms.Button btnNH;
        private System.Windows.Forms.Button btnNV;
        private System.Windows.Forms.Panel pnNV;
        private System.Windows.Forms.Button btnTimNV;
        private System.Windows.Forms.TextBox txtTimNV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnXoaNV;
        private System.Windows.Forms.Button btnSuaNV;
        private System.Windows.Forms.Button btnThemNV;
        private System.Windows.Forms.Button btnXemNV;
        private System.Windows.Forms.DataGridView dgvNV;
        private System.Windows.Forms.DataGridViewTextBoxColumn clMaNV;
        private System.Windows.Forms.DataGridViewTextBoxColumn clHoTenNV;
        private System.Windows.Forms.DataGridViewTextBoxColumn clGioiTinh;
        private System.Windows.Forms.DataGridViewTextBoxColumn clChucVuNV;
        private System.Windows.Forms.Panel pnNH;
        private System.Windows.Forms.Button btnXoaNH;
        private System.Windows.Forms.Button btnSuaNH;
        private System.Windows.Forms.Button btnThemNH;
        private System.Windows.Forms.Button btnXemNH;
        private System.Windows.Forms.DataGridView dgvNH;
        private System.Windows.Forms.Panel pnTK;
        private System.Windows.Forms.Button btnXoaTK;
        private System.Windows.Forms.Button btnSuaTK;
        private System.Windows.Forms.Button btnThemTK;
        private System.Windows.Forms.Button btnXemTK;
        private System.Windows.Forms.DataGridView dgvTK;
        private System.Windows.Forms.DataGridViewTextBoxColumn clIDTK;
        private System.Windows.Forms.DataGridViewTextBoxColumn clNgayTK;
        private System.Windows.Forms.DataGridViewTextBoxColumn clDoanhThu;
        private System.Windows.Forms.Panel pnHH;
        private System.Windows.Forms.Button btnTimHH;
        private System.Windows.Forms.TextBox txtTimHH;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnXoaHH;
        private System.Windows.Forms.Button btnThemHH;
        private System.Windows.Forms.DataGridView dgvHH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clMaHH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clTenHH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clSLHH;
        private System.Windows.Forms.Button btnSuaHH;
        private System.Windows.Forms.Button btnXemHH;
        private System.Windows.Forms.Panel pnHD;
        private System.Windows.Forms.Button btnTimHD;
        private System.Windows.Forms.TextBox txtTimHD;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnXoaHD;
        private System.Windows.Forms.Button btnThemHD;
        private System.Windows.Forms.Button btnSuaHD;
        private System.Windows.Forms.DataGridView dgvHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn clSoHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn clNgayLap;
        private System.Windows.Forms.DataGridViewTextBoxColumn clTongTien;
        private System.Windows.Forms.Button btnXemHD;
        private System.Windows.Forms.DataGridViewTextBoxColumn clMaNH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clNgayNH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clTongTienNH;
    }
}