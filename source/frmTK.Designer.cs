﻿
namespace QuanLyCuaHangDoChoi
{
    partial class frmTK
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAddTK = new System.Windows.Forms.Button();
            this.btnUpdateTK = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.txtDoanhThu = new System.Windows.Forms.TextBox();
            this.txtChi = new System.Windows.Forms.TextBox();
            this.txtThu = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtNgayTK = new System.Windows.Forms.TextBox();
            this.txtMaTK = new System.Windows.Forms.TextBox();
            this.btnKtraTK = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnAddTK
            // 
            this.btnAddTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnAddTK.Location = new System.Drawing.Point(326, 407);
            this.btnAddTK.Name = "btnAddTK";
            this.btnAddTK.Size = new System.Drawing.Size(183, 54);
            this.btnAddTK.TabIndex = 11;
            this.btnAddTK.Text = "Thêm thống kê";
            this.btnAddTK.UseVisualStyleBackColor = true;
            this.btnAddTK.Click += new System.EventHandler(this.btnAddTK_Click);
            // 
            // btnUpdateTK
            // 
            this.btnUpdateTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnUpdateTK.Location = new System.Drawing.Point(51, 407);
            this.btnUpdateTK.Name = "btnUpdateTK";
            this.btnUpdateTK.Size = new System.Drawing.Size(148, 54);
            this.btnUpdateTK.TabIndex = 10;
            this.btnUpdateTK.Text = "Cập nhật";
            this.btnUpdateTK.UseVisualStyleBackColor = true;
            this.btnUpdateTK.Click += new System.EventHandler(this.btnUpdateTK_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.txtDoanhThu);
            this.panel2.Controls.Add(this.txtChi);
            this.panel2.Controls.Add(this.txtThu);
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.label5);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Location = new System.Drawing.Point(12, 213);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(519, 167);
            this.panel2.TabIndex = 9;
            // 
            // txtDoanhThu
            // 
            this.txtDoanhThu.Location = new System.Drawing.Point(173, 120);
            this.txtDoanhThu.Name = "txtDoanhThu";
            this.txtDoanhThu.Size = new System.Drawing.Size(324, 27);
            this.txtDoanhThu.TabIndex = 7;
            // 
            // txtChi
            // 
            this.txtChi.Location = new System.Drawing.Point(173, 67);
            this.txtChi.Name = "txtChi";
            this.txtChi.Size = new System.Drawing.Size(324, 27);
            this.txtChi.TabIndex = 6;
            // 
            // txtThu
            // 
            this.txtThu.Location = new System.Drawing.Point(173, 16);
            this.txtThu.Name = "txtThu";
            this.txtThu.Size = new System.Drawing.Size(324, 27);
            this.txtThu.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(24, 12);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(103, 28);
            this.label3.TabIndex = 2;
            this.label3.Text = "Tổng thu:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(24, 116);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(117, 28);
            this.label5.TabIndex = 4;
            this.label5.Text = "Doanh thu:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(24, 63);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 28);
            this.label4.TabIndex = 3;
            this.label4.Text = "Tổng chi:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txtNgayTK);
            this.panel1.Controls.Add(this.txtMaTK);
            this.panel1.Controls.Add(this.btnKtraTK);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(519, 194);
            this.panel1.TabIndex = 8;
            // 
            // txtNgayTK
            // 
            this.txtNgayTK.Location = new System.Drawing.Point(173, 75);
            this.txtNgayTK.Name = "txtNgayTK";
            this.txtNgayTK.Size = new System.Drawing.Size(324, 27);
            this.txtNgayTK.TabIndex = 4;
            // 
            // txtMaTK
            // 
            this.txtMaTK.Location = new System.Drawing.Point(173, 23);
            this.txtMaTK.Name = "txtMaTK";
            this.txtMaTK.Size = new System.Drawing.Size(324, 27);
            this.txtMaTK.TabIndex = 3;
            // 
            // btnKtraTK
            // 
            this.btnKtraTK.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.btnKtraTK.Location = new System.Drawing.Point(349, 124);
            this.btnKtraTK.Name = "btnKtraTK";
            this.btnKtraTK.Size = new System.Drawing.Size(148, 54);
            this.btnKtraTK.TabIndex = 2;
            this.btnKtraTK.Text = "Kiểm tra";
            this.btnKtraTK.UseVisualStyleBackColor = true;
            this.btnKtraTK.Click += new System.EventHandler(this.btnKtraTK_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(24, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 28);
            this.label2.TabIndex = 1;
            this.label2.Text = "Ngày lập:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.Location = new System.Drawing.Point(24, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(143, 28);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mã thống kê: ";
            // 
            // frmTK
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(542, 472);
            this.Controls.Add(this.btnAddTK);
            this.Controls.Add(this.btnUpdateTK);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "frmTK";
            this.Text = "frmTK";
            this.Load += new System.EventHandler(this.frmTK_Load);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnThongKe;
        private System.Windows.Forms.Button btnUpdateTK;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox txtDoanhThu;
        private System.Windows.Forms.TextBox txtTongChi;
        private System.Windows.Forms.TextBox txtThu;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TextBox txtNgayTK;
        private System.Windows.Forms.TextBox txtMaTK;
        private System.Windows.Forms.Button btnKiemTra;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddTK;
        private System.Windows.Forms.TextBox txtChi;
        private System.Windows.Forms.Button btnKtraTK;
    }
}