﻿
namespace QuanLyCuaHangDoChoi
{
    partial class frmHD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel4 = new System.Windows.Forms.Panel();
            this.tvTongTien = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.btnThanhToan = new System.Windows.Forms.Button();
            this.dgvDonHang = new System.Windows.Forms.DataGridView();
            this.clMaHH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clTenHH = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clDonGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clSoLuong = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.clThanhTien = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnUpdateHD = new System.Windows.Forms.Button();
            this.btnXoaHH = new System.Windows.Forms.Button();
            this.btnThemHH = new System.Windows.Forms.Button();
            this.pnEditHD = new System.Windows.Forms.Panel();
            this.txtNgayHD = new System.Windows.Forms.TextBox();
            this.txtSL = new System.Windows.Forms.TextBox();
            this.tvTenHH = new System.Windows.Forms.Label();
            this.tvThanhTien = new System.Windows.Forms.Label();
            this.tvDonGia = new System.Windows.Forms.Label();
            this.txtMaHH = new System.Windows.Forms.TextBox();
            this.txtSoHD = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDonHang)).BeginInit();
            this.panel3.SuspendLayout();
            this.pnEditHD.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tvTongTien);
            this.panel4.Controls.Add(this.label9);
            this.panel4.Controls.Add(this.btnThanhToan);
            this.panel4.Controls.Add(this.dgvDonHang);
            this.panel4.Location = new System.Drawing.Point(12, 417);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(903, 246);
            this.panel4.TabIndex = 7;
            // 
            // tvTongTien
            // 
            this.tvTongTien.AutoSize = true;
            this.tvTongTien.BackColor = System.Drawing.SystemColors.Control;
            this.tvTongTien.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tvTongTien.Location = new System.Drawing.Point(267, 189);
            this.tvTongTien.Name = "tvTongTien";
            this.tvTongTien.Size = new System.Drawing.Size(0, 38);
            this.tvTongTien.TabIndex = 5;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.Control;
            this.label9.Font = new System.Drawing.Font("Segoe UI", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label9.Location = new System.Drawing.Point(56, 189);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(166, 38);
            this.label9.TabIndex = 4;
            this.label9.Text = "TỔNG TIỀN";
            // 
            // btnThanhToan
            // 
            this.btnThanhToan.Location = new System.Drawing.Point(683, 175);
            this.btnThanhToan.Name = "btnThanhToan";
            this.btnThanhToan.Size = new System.Drawing.Size(141, 52);
            this.btnThanhToan.TabIndex = 3;
            this.btnThanhToan.Text = "THANH TOÁN";
            this.btnThanhToan.UseVisualStyleBackColor = true;
            this.btnThanhToan.Click += new System.EventHandler(this.btnThanhToan_Click);
            // 
            // dgvDonHang
            // 
            this.dgvDonHang.AllowUserToAddRows = false;
            this.dgvDonHang.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDonHang.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.clMaHH,
            this.clTenHH,
            this.clDonGia,
            this.clSoLuong,
            this.clThanhTien});
            this.dgvDonHang.Location = new System.Drawing.Point(11, 14);
            this.dgvDonHang.Name = "dgvDonHang";
            this.dgvDonHang.ReadOnly = true;
            this.dgvDonHang.RowHeadersWidth = 51;
            this.dgvDonHang.RowTemplate.Height = 29;
            this.dgvDonHang.Size = new System.Drawing.Size(875, 155);
            this.dgvDonHang.TabIndex = 0;
            this.dgvDonHang.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvDonHang_CellClick);
            // 
            // clMaHH
            // 
            this.clMaHH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clMaHH.DataPropertyName = "MaHH";
            this.clMaHH.HeaderText = "Mã";
            this.clMaHH.MinimumWidth = 6;
            this.clMaHH.Name = "clMaHH";
            this.clMaHH.ReadOnly = true;
            this.clMaHH.Width = 59;
            // 
            // clTenHH
            // 
            this.clTenHH.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clTenHH.DataPropertyName = "TenHH";
            this.clTenHH.HeaderText = "Tên hàng hóa";
            this.clTenHH.MinimumWidth = 6;
            this.clTenHH.Name = "clTenHH";
            this.clTenHH.ReadOnly = true;
            this.clTenHH.Width = 127;
            // 
            // clDonGia
            // 
            this.clDonGia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clDonGia.DataPropertyName = "DonGia";
            this.clDonGia.HeaderText = "Giá";
            this.clDonGia.MinimumWidth = 6;
            this.clDonGia.Name = "clDonGia";
            this.clDonGia.ReadOnly = true;
            this.clDonGia.Width = 60;
            // 
            // clSoLuong
            // 
            this.clSoLuong.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.clSoLuong.DataPropertyName = "SoLuong";
            this.clSoLuong.HeaderText = "Số lượng";
            this.clSoLuong.MinimumWidth = 6;
            this.clSoLuong.Name = "clSoLuong";
            this.clSoLuong.ReadOnly = true;
            this.clSoLuong.Width = 98;
            // 
            // clThanhTien
            // 
            this.clThanhTien.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.clThanhTien.DataPropertyName = "ThanhTien";
            this.clThanhTien.HeaderText = "Thành tiền";
            this.clThanhTien.MinimumWidth = 6;
            this.clThanhTien.Name = "clThanhTien";
            this.clThanhTien.ReadOnly = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnUpdateHD);
            this.panel3.Controls.Add(this.btnXoaHH);
            this.panel3.Controls.Add(this.btnThemHH);
            this.panel3.Location = new System.Drawing.Point(12, 332);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(903, 79);
            this.panel3.TabIndex = 6;
            // 
            // btnUpdateHD
            // 
            this.btnUpdateHD.Location = new System.Drawing.Point(683, 11);
            this.btnUpdateHD.Name = "btnUpdateHD";
            this.btnUpdateHD.Size = new System.Drawing.Size(141, 52);
            this.btnUpdateHD.TabIndex = 2;
            this.btnUpdateHD.Text = "CẬP NHẬT";
            this.btnUpdateHD.UseVisualStyleBackColor = true;
            this.btnUpdateHD.Click += new System.EventHandler(this.btnUpdateHD_Click);
            // 
            // btnXoaHH
            // 
            this.btnXoaHH.Location = new System.Drawing.Point(398, 11);
            this.btnXoaHH.Name = "btnXoaHH";
            this.btnXoaHH.Size = new System.Drawing.Size(141, 52);
            this.btnXoaHH.TabIndex = 1;
            this.btnXoaHH.Text = "XÓA";
            this.btnXoaHH.UseVisualStyleBackColor = true;
            this.btnXoaHH.Click += new System.EventHandler(this.btnXoaHH_Click);
            // 
            // btnThemHH
            // 
            this.btnThemHH.Location = new System.Drawing.Point(95, 11);
            this.btnThemHH.Name = "btnThemHH";
            this.btnThemHH.Size = new System.Drawing.Size(141, 52);
            this.btnThemHH.TabIndex = 0;
            this.btnThemHH.Text = "THÊM";
            this.btnThemHH.UseVisualStyleBackColor = true;
            this.btnThemHH.Click += new System.EventHandler(this.btnThemHH_Click);
            // 
            // pnEditHD
            // 
            this.pnEditHD.Controls.Add(this.txtNgayHD);
            this.pnEditHD.Controls.Add(this.txtSL);
            this.pnEditHD.Controls.Add(this.tvTenHH);
            this.pnEditHD.Controls.Add(this.tvThanhTien);
            this.pnEditHD.Controls.Add(this.tvDonGia);
            this.pnEditHD.Controls.Add(this.txtMaHH);
            this.pnEditHD.Controls.Add(this.txtSoHD);
            this.pnEditHD.Controls.Add(this.label8);
            this.pnEditHD.Controls.Add(this.label7);
            this.pnEditHD.Controls.Add(this.label6);
            this.pnEditHD.Controls.Add(this.label5);
            this.pnEditHD.Controls.Add(this.label4);
            this.pnEditHD.Controls.Add(this.label3);
            this.pnEditHD.Controls.Add(this.label2);
            this.pnEditHD.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.pnEditHD.Location = new System.Drawing.Point(12, 87);
            this.pnEditHD.Name = "pnEditHD";
            this.pnEditHD.Size = new System.Drawing.Size(903, 238);
            this.pnEditHD.TabIndex = 5;
            // 
            // txtNgayHD
            // 
            this.txtNgayHD.Location = new System.Drawing.Point(578, 19);
            this.txtNgayHD.Name = "txtNgayHD";
            this.txtNgayHD.Size = new System.Drawing.Size(213, 34);
            this.txtNgayHD.TabIndex = 15;
            // 
            // txtSL
            // 
            this.txtSL.Location = new System.Drawing.Point(578, 130);
            this.txtSL.Name = "txtSL";
            this.txtSL.Size = new System.Drawing.Size(213, 34);
            this.txtSL.TabIndex = 14;
            this.txtSL.TextChanged += new System.EventHandler(this.txtSL_TextChanged);
            // 
            // tvTenHH
            // 
            this.tvTenHH.AutoSize = true;
            this.tvTenHH.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tvTenHH.Location = new System.Drawing.Point(578, 85);
            this.tvTenHH.Name = "tvTenHH";
            this.tvTenHH.Size = new System.Drawing.Size(0, 28);
            this.tvTenHH.TabIndex = 13;
            // 
            // tvThanhTien
            // 
            this.tvThanhTien.AutoSize = true;
            this.tvThanhTien.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tvThanhTien.Location = new System.Drawing.Point(161, 196);
            this.tvThanhTien.Name = "tvThanhTien";
            this.tvThanhTien.Size = new System.Drawing.Size(0, 28);
            this.tvThanhTien.TabIndex = 12;
            // 
            // tvDonGia
            // 
            this.tvDonGia.AutoSize = true;
            this.tvDonGia.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.tvDonGia.Location = new System.Drawing.Point(161, 138);
            this.tvDonGia.Name = "tvDonGia";
            this.tvDonGia.Size = new System.Drawing.Size(0, 28);
            this.tvDonGia.TabIndex = 11;
            // 
            // txtMaHH
            // 
            this.txtMaHH.Location = new System.Drawing.Point(161, 79);
            this.txtMaHH.Name = "txtMaHH";
            this.txtMaHH.Size = new System.Drawing.Size(213, 34);
            this.txtMaHH.TabIndex = 8;
            this.txtMaHH.Leave += new System.EventHandler(this.txtMaHH_Leave);
            // 
            // txtSoHD
            // 
            this.txtSoHD.Location = new System.Drawing.Point(161, 19);
            this.txtSoHD.Name = "txtSoHD";
            this.txtSoHD.Size = new System.Drawing.Size(213, 34);
            this.txtSoHD.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label8.Location = new System.Drawing.Point(19, 196);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(114, 28);
            this.label8.TabIndex = 6;
            this.label8.Text = "Thành tiền";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label7.Location = new System.Drawing.Point(414, 136);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(102, 28);
            this.label7.TabIndex = 5;
            this.label7.Text = "Số lượng:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label6.Location = new System.Drawing.Point(19, 136);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(87, 28);
            this.label6.TabIndex = 4;
            this.label6.Text = "Đơn giá";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label5.Location = new System.Drawing.Point(414, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(144, 28);
            this.label5.TabIndex = 3;
            this.label5.Text = "Tên hàng hóa:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label4.Location = new System.Drawing.Point(19, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 28);
            this.label4.TabIndex = 2;
            this.label4.Text = "Mã hàng hóa:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label3.Location = new System.Drawing.Point(414, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(102, 28);
            this.label3.TabIndex = 1;
            this.label3.Text = "Ngày lập:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label2.Location = new System.Drawing.Point(19, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(125, 28);
            this.label2.TabIndex = 0;
            this.label2.Text = "Số hóa đơn:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(903, 68);
            this.panel1.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.label1.ForeColor = System.Drawing.Color.DeepSkyBlue;
            this.label1.Location = new System.Drawing.Point(267, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(372, 41);
            this.label1.TabIndex = 0;
            this.label1.Text = "HÓA ĐƠN THANH TOÁN";
            // 
            // frmHD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(928, 672);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.pnEditHD);
            this.Controls.Add(this.panel1);
            this.Name = "frmHD";
            this.Text = "frmHD";
            this.Load += new System.EventHandler(this.frmHD_Load);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDonHang)).EndInit();
            this.panel3.ResumeLayout(false);
            this.pnEditHD.ResumeLayout(false);
            this.pnEditHD.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lbTongTien;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnThanhToan;
        private System.Windows.Forms.DataGridView dgvDonHang;
        private System.Windows.Forms.DataGridViewTextBoxColumn clMaHH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clTenHH;
        private System.Windows.Forms.DataGridViewTextBoxColumn clDonGia;
        private System.Windows.Forms.DataGridViewTextBoxColumn clSoLuong;
        private System.Windows.Forms.DataGridViewTextBoxColumn clThanhTien;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button btnCapNhatHoaDon;
        private System.Windows.Forms.Button btnXoaHH;
        private System.Windows.Forms.Button btnThemHH;
        private System.Windows.Forms.Panel pnEditHD;
        private System.Windows.Forms.TextBox txtNgayHD;
        private System.Windows.Forms.TextBox txtSoLuong;
        private System.Windows.Forms.Label tvTenHH;
        private System.Windows.Forms.Label tvThanhTien;
        private System.Windows.Forms.Label tvDonGia;
        private System.Windows.Forms.TextBox txtMaHH;
        private System.Windows.Forms.TextBox txtSoHD;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnUpdateHD;
        private System.Windows.Forms.Label tvTongTien;
        private System.Windows.Forms.TextBox txtSL;
    }
}